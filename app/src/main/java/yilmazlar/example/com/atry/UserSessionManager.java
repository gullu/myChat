package yilmazlar.example.com.atry;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.widget.Toast;

import java.util.HashMap;


public class UserSessionManager {


    private static final String PREFER_NAME = "AndroidExamplePref";
    int PRIVATE_MODE = 0;
    private  static final String IS_USER_LOGIN = "IsUserLoggedIn";
    Context _context;
    SharedPreferences prefences;
    Editor editor;
    public static final String username = "name";


    public UserSessionManager(Context context){
        this._context = context;
        prefences = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = prefences.edit();
    }

    public void createUserLoginSession(String name ){
        // Storing login value as TRUE
        editor.putBoolean(IS_USER_LOGIN, true);
        // Storing name in pref
        editor.putString(username, name);
        // commit changes
        editor.commit();
    }

    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        //Use hashmap to store user credentials
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(username, prefences.getString(username, null));

        // return user
        return user;
    }

    public boolean checkLogin(){
        if (this.isUserLoggedIn()){

            Intent i = new Intent(_context,NavigationActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);

            return true;
        }
        return false;
    }



    public void logoutUser(){
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context,Login.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
        Toast.makeText(_context, "oturum kapatılıyor...", Toast.LENGTH_SHORT).show();
    }
    public boolean isUserLoggedIn(){
        return prefences.getBoolean(IS_USER_LOGIN,false);
    }
}

