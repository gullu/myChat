package yilmazlar.example.com.atry;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;


public class Login extends AppCompatActivity {

    SharedPreferences prefences;
    SharedPreferences.Editor editor;
    private static final String PREFER_NAME = "AndroidExamplePref";
    int PRIVATE_MODE = 0;
    private  static final String IS_USER_LOGIN = "IsUserLoggedIn";
    UserSessionManager oturum;

    private EditText editTextUserName;
    private EditText editTextUserPassword;
    private Button buttonLogin;
    private TextView txtRegister;
    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private String userName;
    private String userPassword;
    private DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        oturum = new UserSessionManager(getApplicationContext());
        prefences = getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = prefences.edit();


        editTextUserName = (EditText)findViewById(R.id.username);
        editTextUserPassword = (EditText)findViewById(R.id.password);
        buttonLogin = (Button) findViewById(R.id.loginButton);
        txtRegister = (TextView) findViewById(R.id.register);

        mAuth = FirebaseAuth.getInstance();
        firebaseUser = mAuth.getCurrentUser(); // authenticated user

/*
        if(firebaseUser != null){ // check user session


            Intent i = new Intent(Login.this,NavigationActivity.class);
            startActivity(i);
            Toast.makeText(getApplicationContext(),
                    "Oturum Açılıyor... " ,
                    Toast.LENGTH_SHORT).show();

            finish();

        }
*/
        if(oturum.checkLogin()){
            Toast.makeText(getApplicationContext(),
                    "Oturum Açılıyor... " ,
                    Toast.LENGTH_SHORT).show();

            finish();
        }



        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userName = editTextUserName.getText().toString();
                userPassword = editTextUserPassword.getText().toString();
                if(userName.isEmpty() || userPassword.isEmpty()){

                    Toast.makeText(getApplicationContext(),"Lütfen gerekli alanları doldurunuz!",Toast.LENGTH_SHORT).show();

                }else{


                    Toast.makeText(getApplicationContext(), "Kayıt Yapıldı.",Toast.LENGTH_LONG).show();


                    loginFunc();
                }
            }
        });

        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this,Register2.class);
                startActivity(intent);
            }
        });
    }

    private void loginFunc() {

        mAuth.signInWithEmailAndPassword(userName,userPassword).addOnCompleteListener(Login.this,
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){

                            String user = task.getResult().getUser().getEmail();
                            oturum.createUserLoginSession(user);


                         /*  String name = task.getResult().getUser().getEmail();
                            editor.putBoolean(IS_USER_LOGIN, true);
                            editor.putString("name", name);
                            editor.commit();


                            Toast.makeText(Login.this, name, Toast.LENGTH_SHORT).show();*/
                            Intent i = new Intent(Login.this,NavigationActivity.class);
                            startActivity(i);
                            finish();

                            Toast.makeText(getApplicationContext(),
                                    "Oturum Açıldı... ",
                                    Toast.LENGTH_SHORT).show();

                        }
                        else{
                            // hata
                            Toast.makeText(getApplicationContext(),task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }

                });
    }



}